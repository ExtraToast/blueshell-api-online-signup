INSERT INTO blueshell.news (creator_id, last_editor_id, news_type, title, content, posted_at) VALUES (1, 1, 'News', 'Brace yourselves – Summer is coming', '<div><p>If you haven’t noticed yet with the many BBQ’s being organized throughout the campus, the wonderful weather we are having or  your study demanding your last breath of effort for these final few weeks, the summer holidays are approaching! What does this mean for Blueshell? Well, it means this is your chance to already get a bit of the holiday feeling and relax at a few of our activities (hint: sign-up can happen somewhere here at the site). Moreover, we hope that at the end of this month, we can announce the first candidate board for Blueshell! These will take over the reins after the summer holidays, and all of the members can expect an invite for the official Blueshell GMM (general members meeting).</p>
<p>But let’s not look too far forward, there is still plenty to do. We as a board are still interested in looking to improve the Discord (our main communication channel), and we are glad a few members have offered their help, seeing as the board is not exactly ‘’experienced’’ with this wonderful platform. Moreover, there are still plenty of ideas we would like to take up, so don’t be surprised when you see some brand new idea announcements on the Discord! Moreover, don’t forget Blueshell runs on its members – don’t hesistate to contact us to start organizing something, and when you think about starting up a solo game of LoL or climb the ladder in Hearthstone, ask in the channel if anyone is up for some fun! We wish you all the best in these last few weeks of the year, good luck with your studies, and take (a deserved) break of those same studies to come to our activities. </p>
<p>See you soon!</p>
</div>', '2018-05-10 21:42:51');
INSERT INTO blueshell.news (creator_id, last_editor_id, news_type, title, content, posted_at) VALUES (1, 1, 'News', 'Candidate Board & First year of Blueshell', '<div><p>With summer approaching, so does the end of the first year of Blueshell E-sports. As a board, we are very content with the current state of Blueshell, having a healthy memberbase, great potential to grow and most importantly: a lot of fun for everyone interested in gaming. Moreover, where other associations may struggle to find a candidate board, we immeadiately had some members jump at the opportunity, and ended up with five enthusaistic gamers that will carry on (and create) Blueshell’s legacy:</p>
<p><em>Jasper van Harten as Chairman</em></p>
<p><em>Kimberly Evertsz as Secretary</em></p>
<p><em>Mauk Muller as Treasurer</em></p>
<p><em>Antal van Dongen as Commissioner of Internal Affairs</em></p>
<p><em>Maiander Eigenraam as Commissioner of External affairs</em></p>
<p>Don’t worry you won’t see us again till next year! While our (local) activities might not be as regular as normally, the summer holidays are an excellent opportunity to make some new friends! So hop on our Discord, ask around a bit for people to play with and jump into a game with a fellow student you didn’t know before!</p>
<p>We wish you all the best with finishing up your academic year!</p>
</div>', '2018-06-14 06:09:40');
INSERT INTO blueshell.news (creator_id, last_editor_id, news_type, title, content, posted_at) VALUES (1, 1, 'News', 'New cooperation El Nino!', '<div><p>Dear Blueshellers,</p>
<p>We are proud to announce that Blueshell has started a new professional cooperation with El Niño, a firm from Enschede specialized in digitization and automation. Different from other large firms, El Niño has a very personal approach, delivering custom solutions to complex IT problems. For us, their support allows us to continue to establish an E-sports and gaming community in Twente.<br>
If you are interested in the firm, or if you would like to come into contact with them, be sure to visit <a href="https://www.elnino.tech/">their site</a>! We look forward to a successful year for both Blueshell and El Niño.</p>
<p><a href="https://www.elnino.tech/"></a></p>
</div>', '2018-07-14 06:09:40');
INSERT INTO blueshell.news (creator_id, last_editor_id, news_type, title, content, posted_at) VALUES (1, 1, 'News', 'Looking for players for our League of Legends teams!', '<div><p>Are you looking to take your game to the next level? Do you want to represent Blueshell Esports in League of Legends tournaments? Then you came to the right place! If you’re Diamond 4+ this season as well, you can tryout for the BS Primus team that will participate in the Dutch College League for League of Legends!</p>
<p><a href="https://docs.google.com/forms/d/e/1FAIpQLSemOJBVMkUvaTfDlCkrQlMwTeFJ_pmb-YEevGP1xIau397_oA/viewform">Click here to sign up!</a></p>
</div>', '2019-09-08 21:50:06');
INSERT INTO blueshell.news (creator_id, last_editor_id, news_type, title, content, posted_at) VALUES (1, 1, 'News', 'Post event feedback: 4th of October LoL Tournament', '<div><p>It’s been two weeks since the League of Legends tournament on 4th of October happened and it’s time to look back and reflect upon it. The event was a big success, with over 80 people signing up and 16 teams in total. This was actually the biggest League tourney we’ve ever had so far in the history of Blueshell and we hope the next ones are gonna be even bigger! We would also like to congratulate again BS Primus for winning the tournament!</p></div>', '2019-10-19 21:48:10');
INSERT INTO blueshell.news (creator_id, last_editor_id, news_type, title, content, posted_at) VALUES (1, 1, 'News', 'BS Primus wins 1-0 against Zephyr Nox!', '<div><p>Our League of Legends team, BS Primus, has won another match in the Premier League of Dutch College League against Zephyr Nox! We are currently 2-0 and first in our groups! Lets go boys!</p></div>', '2019-12-16 21:47:37');
INSERT INTO blueshell.news (creator_id, last_editor_id, news_type, title, content, posted_at) VALUES (1, 1, 'News', 'Upcoming League of Legends Tournament on the 28th of February', '<div><p>Blueshell Esports is organizing another League of Legends 5v5 tournament on the 28th of February! For this occasion, we are also doing promotion stands in front of Ravelijn and Horst on the 3rd and 4th of February. Come over during the lunch break to say hi to the committee and for some free hot Chocomel! For more info about the tournament, please go to our Events page.</p>
</div>', '2020-02-03 21:45:59');
INSERT INTO blueshell.news (creator_id, last_editor_id, news_type, title, content, posted_at) VALUES (1, 1, 'News', 'WIRWAR – LAN party at the FC Twente Stadium!', '<div><div><div>
<div>
	<h5><strong>What is WIRWAR?</strong></h5>
<p>On the 9<sup>th</sup> of October 2020 something special will happen at the FC Twente stadium. This is the moment when the first edition of the <strong>WIRWAR LAN party</strong> will be held. During the Lan party, you can experience competitive gaming, console gaming, retro games, and VR games in a Total Reality room. The event will start at 15:00 and will end in the late evening. This all will create a gaming atmosphere unique for the region of Twente mixing gaming enthusiasts from regional companies and students from the UT and Saxion! Additionally, representatives from the Esportslab of the University of Twente and the Esports Team Twente will also be present.</p>
</div>
</div></div><div><div>
<div>
	<h5><strong></strong><strong>How do I register?</strong></h5>
<p>For more information and registration, follow this <a href="http://wirwar.gg/" target="_blank" rel="noopener noreferrer">link </a>to WIRWAR''s official website. Moreover, for student visitors, there is a discount available when you use the following code in the registration form: <strong>WRWRstudent.</strong></p>
<h5><strong></strong></h5>
</div>
</div></div></div>', '2020-09-19 21:45:15');
INSERT INTO blueshell.news (creator_id, last_editor_id, news_type, title, content, posted_at) VALUES (6, 6, 'Interview', 'Meet Willow: Enschede’s Finest and Most Influential Cosplayer in the Gaming Community', '<div>
  <img src="https://esa-blueshell.nl/api/download/willow1.jpg" class="ma-4"
       style="max-width: 40%;float: right" alt="Cosplay photo"/>
  <p class="body-1">From Illidan Stormrage to Mewtwo, with full-body suits and gigantic props, she
    does it all.
    Willow is a professional cosplayer, someone who has dedicated their entire life to the arts
    and crafts of cosplay: a performance art in which people dress up as their favorite characters
    from games, movies, or comics. She lives in Enschede and runs her own cosplay business called
    Willow Creative. With no less than 96.000 followers on Instagram, she makes a big impact in
    the cosplay world with her creative outfits.
  </p>


  <p class="text-h5">Who is Willow?</p>
  <p class="body-1">
    I am Merel Eisink, 28 years old and I live in Enschede and I am a professional cosplayer. I
    sell cosplay accessories such as masks or costumes. I started this venture three years ago
    with my own company Willow Creative.
  </p>
  <p class="body-1">
    When I was 20 years old, I studied Creative Media and Game Technologies at HBO Saxion
    Hogeschool Enschede. During my study time, I was already involved in cosplay, gaming and was a
    huge fan of League of Legends. I chose this study based on my hobbies and interests (cosplay
    and gaming).
  </p>
  <p class="body-1">
    During my study, I had a small side hustle where I 3D-printed my own designed masks. I posted
    it online and people liked my cosplay, how they are being built or how the design processes
    are done. They can see how their favorite game characters are coming to life, from 3D
    sketching towards a realistic and physical design.
  </p>
  <p class="quote">
    "I am really trying to bring the fantasy of the game and characters into our reality."
  </p>
  <p class="body-1">
    You also learn how to develop and design a game with my study, but that was a bit superficial:
    everything is either a big flat screen or in code. With cosplay, you can do more than only
    visual design on-screen: moving parts, LED lights, and robotics. I am really trying to bring
    the fantasy of the game into our reality. This is probably the most fun and challenging aspect
    of cosplay.
  </p>


  <p class="text-h5">What kinds of techniques do you use to create your cosplays?</p>
  <p class="body-1">
    My toolkit has grown over the last couple of years. Obviously, in the beginning, you only have
    a precision knife, glue, and some paper material. Nowadays, I have multiple 3D printers to
    create my work. I also got a laser cutter that I use for cutting foam or vinyl more easily. I
    make my own 2D and 3D patterns for sewing and cutting.
  </p>
  <p class="quote">
    "I created my entire body in 3D so I can easily make cosplay for myself."
  </p>
  <p class="body-1">
    One technique that I myself have created for creating my cosplays, is that I created my entire
    body in 3D so that I can easily make cosplay for myself. Normally, you take scissors and try
    to cut cloth and material until it fits. Now I can do this all digitally. This saves more time
    for me to create new designs or cosplays.
  </p>


  <p class="text-h5">How did you make cosplaying your profession and what were the obstacles?</p>

  <img src="https://esa-blueshell.nl/api/download/willow2.jpg" class="ma-4"
       style="max-width: 40%;float: left" alt="Cosplay photo"/>
  <p class="body-1">
    While I was studying at Saxion, some people were interested in my cosplay
    props via friends or social media. Back then, I made a pistol from D.Va from Overwatch for
    someone. I discovered through my study that I enjoyed making physical props rather than
    becoming a game designer. I also like to work on my own.
  </p>
  <p class="body-1">
    One big obstacle to going professional is the time investment in tailor-made props. Once you
    spend a lot of time on the designing and prototyping of such props, you only made it for one
    person, which is economically inefficient.
  </p>
  <p class="body-1">
    With the advent of 3D printing machines, I can now easily design horns and masks and sell them
    en masse. I sell hundreds of them every year. This made me decide to make my hobby a full-time
    job.
  </p>

  <p class="text-h5">What are the advantages and disadvantages of being a cosplayer?</p>
  <p class="body-1">
    Before corona, I would attend conventions once or twice a month. There are a lot of
    disadvantages when you are wearing big and bulky cosplay. Dragging along all the equipment all
    day long is a hassle. Also, visitors that don’t know how to behave near cosplay outfits,
    thinking that they are allowed to touch our costumes or don’t know some parts are vulnerable.
  </p>
  <p class="quote">
    "I sometimes make props for gaming companies such as Jagex (Runescape), Blizzard (World of
    Warcraft), and Riot Games (League of Legends)."
  </p>
  <p class="body-1">
    Connecting with other people on the internet with a passion for cosplay is also really fun and
    great. With that exposure, lots of big companies are contacting me for promotion campaigns and
    to sponsor products. I sometimes make props for gaming companies such as Jagex (Runescape),
    Blizzard (World of Warcraft). Also, doing this work individually offers me the freedom that I
    like. I mostly have three or four projects running at the same time, picking up projects
    whenever I feel like it.
  </p>

  <p class="text-h5">What is your favorite type of costume to create? </p>
  <p class="body-1">
    It’s hard to say because I like to craft or create something new. If I am making the same sort
    of body armor or pauldrons, then the fun’s gone. I prefer always designing something new. It
    can be anything: wings, suits, etc. Sometimes I stick to one aspect, but after a few versions
    or variations, I will challenge myself and look towards other new components that I can make.
  </p>
</div>
', '2022-03-06 14:50:03');
INSERT INTO blueshell.news (creator_id, last_editor_id, news_type, title, content, posted_at) VALUES (6, 6, 'Interview', 'All Grand Masters Play Chess on Enschede Chessboards','<div>
  <p class="body-1">
    Enschede is a very humble and rural city. Away from the busy Randstad with Amsterdam and The
    Hague as pillars of internationalization and globalization and close to the German border,
    Enschede is not well-known for having such ambitions.
  </p>

  <img src="https://esa-blueshell.nl/api/download/dgt1.jpg" class="ma-4"
       style="max-width: 40%;float: left" alt="big chess game"/>

  <p class="body-1">
    However, we play a very important role in the world of chess with Digital Game Technology (DGT).
    If you travel from the campus to the city centre or vice-versa, you can see their unapparent
    logo on a sign behind a white building. They are the company providing chessboards and digital
    clocks for the world’s largest chess events in the world such as the Tata Steel tournaments.
    Every grandmaster chess player plays on a board from Enschede.
  </p>
  <p class="body-1">
    We sit with Hans Pees, who is the CEO of DGT. We ask him how a small company located in Enschede
    plays an important role on the world stage of chess.
  </p>

  <p class="text-h5">
    Who is Hans Pees?
  </p>
  <p class="body-1">
    I am Hans Pees and am current CEO of Digital Game Technologies. Before I took over this company,
    I graduated in Mathematics, lived in England, worked in the transport industry, and have lived a
    couple years in Spain. When I decided to return to the Netherlands, I was looking for a job, any
    job.
  </p>


  <img src="https://esa-blueshell.nl/api/download/dgt2.jpg" class="mb-2"
       style="width: 100%;" alt="Picture of Hans Pees"/>
  <p class="quote">
    After buying 10 clocks, I spoke the legendary words: “You guys got any job applications open?”
  </p>
  <p class="body-1">
    In the meantime, I was playing for Chess Club Wierden. They asked if someone could buy a bundle
    of digital clocks for the association. I said: I could do that. I drove to Enschede – I also
    didn''t know DGT was in Enschede! – and after buying 10 clocks, I spoke the legendary words: “You
    guys got any job applications open?”. The rest was history.
  </p>

  <p class="text-h5">
    How did DGT become so big and important? And what technological innovations are made to stay
    ahead of the competition?
  </p>
  <p class="body-1">
    The founders started their venture when they were UT students. They have worked well over 20
    years in close contact with grand chess organisations. When they made the digital clocks and
    chessboards, we were in early age of digitalization and had the first mover advantage. That’s
    how DGT became the market leader.
  </p>


  <img src="https://esa-blueshell.nl/api/download/dgt3.jpg" class="mb-4"
       style="width": 100%;" alt="Workplace"/>
  <p class="quote">
    Where the magic happens!
  </p>
  <p class="body-1">
    When I took over the company, I was more focused on professionalization the company’s brand.
    When the firm started as a group of friends, you could also see this apparent in the business
    structure: nice, easy, and organic – which is okay! But here I saw possibilities to really
    improve, professionalize, and focus our business.
  </p>
  <p class="body-1">
    Together with our Technology Team, we are searching for new techniques that we can use to
    improve our clocks and boards. We also are looking at what people want and their demand, rather
    than using the newest technologies to create a product.
  </p>
  <p class="body-1">
    Having this first position all boils down innovations. The digital clock: it was so
    revolutionary back in the day that we still hold the advantage. There is not much innovation
    into anymore, however. In 1998, we produced and sold digital chessboards for the competitive
    market. Before the age of the Internet, you could subscribe to a “chess informator” that would
    send you a book every 3 months where you can all the parties and positions played. Now you can
    see live tournaments directly from your phone. This is possible because of the Internet and us!
  </p>
  Are you also involved in local initiatives such as the UT or Twente region?
  <p class="body-1">
    We sometimes are involved. This year we sponsored the national youth chess tournaments. But most
    of all, we are globally promoting chess. It is more ideological in the sense that everyone
    should play chess! For students, we do have internships here, oftentimes technical students and
    sometimes marketing students from both the UT and Saxion Hogeschool. Ever since I have
    professionalized the firm, we have three managers now (innovation, sales, operations). Together,
    we try to run the business
  </p>

  <p class="text-h5">
    What do you think about the digitalization of chess taking place and Grand Chessmasters becoming
    member of Esports associations? Is chess an Esports?
  </p>
  <p class="body-1">
    Of course it is an Esports! Next to the physical chess that we call Over the Board (OTB)¸ there
    is a whole online world where people play chess. We from the OTB side because we make hardware.
    Ultimately for me, chess is a board where you sit opposite of each other, look each other into
    the eyes, do some psychology.
  </p>
  <p class="body-1">
    But with the rise of the PC and mobile, you can play chess against anyone online. That’s good
    for chess, because you can''t do that with many sports. Those two worlds don''t bite. With Corona
    a lot of people were drawn to it, because chess was the only sport that was still played. You
    couldn''t watch soccer matches but you could still watch chess matches.
  </p>


  <img src="https://esa-blueshell.nl/api/download/dgt4.jpg" class="mb-2"
       style="width: 100%;" alt="Bob vs Hans"/>
  <p class="quote">
    Our own champion Bob Even battling with the CEO of Chess! (spoiler: he won!)
  </p>
  <p class="text-h5">
    How did the pandemic affected DGT?
  </p>
  <p class="body-1">
    When corona erupted, all tournaments were cancelled worldwide, and all chess clubs closed as
    well. When those were all closed, it affected our sales of chess clocks. That''s a big market for
    us. The clocks and the electronic chess boards also declined in sales. Until at the end of the
    year Queen''s Gambit was released on Netflix. That did have huge impact on chess in general and
    people wanted to pick up chess clocks again. Chess is not for nerds, but you can see a
    rejuvenation worldwide: chess is cool!
  </p>
</div>', '2022-04-06 18:42:33');


